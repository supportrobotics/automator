var express = require("express");
var app     = express();

var PORT    = 5000;

var mClientSequenceId = {};
var mDeviceSequenceId = {};

var mClientHandle     = {};
var mDeviceHandle     = {};

var mClientRequest    = {};
var mDeviceResponse   = {};

app.use(function(req, res, next)
{
    console.log("use");
    var data = '';

    req.on('data', function(chunk)
    { 
        console.log("use, chunk: " +chunk);
        data += chunk;
    });

    req.on('end', function()
    {
        console.log("use, end");      
        req.payload = data;
        next();
    });
});

 /* serves main page */
 app.get("/", function(aRequest, aResponse)
 {
      var path = __dirname + "/www/index.htm";
      aResponse.sendFile(path);
 });

 // Responses are sent by the device and forwarded to the client browser.
 // Format: /response/<device-id>/<sequence-id>
 //
 app.post(/^\/response\/([^\/]*)\/(.*)/, function(aRequest, aResponse)
 { 
    console.log('Response Request - Params: ' + JSON.stringify(aRequest.params));
    console.log('Response Url:     ' + aRequest.url);

    var deviceId   = aRequest.params[0];
    var sequenceId = aRequest.params[1];
    var payload    = aRequest.payload;

    console.log('Response Device:  ' +deviceId);
    console.log('Response Seq:     ' +sequenceId);
    console.log('Response Payload: ' +payload);   

    processDeviceResponse(deviceId, sequenceId, payload, aResponse);
 });

 // Requests are sent by the client browser and are forwarded to the device
 // Format: /request/<device-id>/<sequence-id>/<command...>
 //
 //app.get(/^\/request\/([^\/]*)\/([^\/]*)\/(.*)/, function(aRequest, aResponse)
 app.post(/^\/request\/([^\/]*)\/(.*)/, function(aRequest, aResponse)
 { 
    //console.log('Request Params: ' + JSON.stringify(aRequest.params));
    console.log('Request Url:    ' + aRequest.url);

    var deviceId   = aRequest.params[0];
    var sequenceId = aRequest.params[1];
    //var command    = aRequest.params[2];
    var command    = aRequest.payload;


    console.log('Request Device:  ' +deviceId);
    console.log('Request Seq:     ' +sequenceId);
    console.log('Request Cmd:     ' +command);

    processClientRequest(aRequest.method, deviceId, sequenceId, command, aResponse);
 });


// Serves all the static files 
//
app.get(/^(.+)$/, function(aRequest, aResponse)
{ 
    console.log('static file request : ' + JSON.stringify(aRequest.params));

    var path = __dirname + "/www/" + aRequest.params[0];

    console.log('static file path: ' + path);

    aResponse.sendFile(path); 
});

var port = process.env.PORT || PORT;

app.listen(port, function()
{
    console.log("Listening on " + port);
});


function processClientRequest(aMethod, aDeviceId, aSequenceId, aCommand, aClientHandle)
{
    var currentClientSeqId = mClientSequenceId[aDeviceId];
    var currentDeviceSeqId = mDeviceSequenceId[aDeviceId];

    var clientRequestPayload = makeCommand(aMethod, aSequenceId, aCommand);

    mClientHandle[aDeviceId]     = aClientHandle;
    mClientSequenceId[aDeviceId] = aSequenceId;
    mClientRequest[aDeviceId]    = clientRequestPayload;

    if (currentDeviceSeqId == null)
    {
        console.log("processClientRequest, No device has registered yet: " +aSequenceId);        
        return;        
    }

    // Check if we already have the response from the device for this request.
    // If so, immediately send it back to client
    //
    if (currentDeviceSeqId == aSequenceId)
    {
        console.log("Request, Already have response from client: " +aSequenceId);

        var responsePayload = mDeviceResponse[aDeviceId];

        aClientHandle.send(responsePayload);

        mClientHandle[aDeviceId] = null;        
        return;        
    }    

    // currentDeviceSeqId != aSequenceId
    // 
    // This request has not been sent to the device. If we have a waiting handle
    // from the device we can forward it immediately.
    //
    var deviceHandle = mDeviceHandle[aDeviceId];

    if (deviceHandle != null)
    {
        console.log("processClientRequest, Device is waiting for request: " +aSequenceId);
        
        deviceHandle.send(clientRequestPayload);

        mDeviceHandle[aDeviceId] = null;
        return;
    }

    console.log("processClientRequest, No device for request");
}

function processDeviceResponse(aDeviceId, aSequenceId, aPayload, aDeviceHandle)
{
    mDeviceHandle[aDeviceId]     = aDeviceHandle;
    mDeviceSequenceId[aDeviceId] = aSequenceId;
    mDeviceResponse[aDeviceId]   = aPayload;

    var currentClientSeqId = mClientSequenceId[aDeviceId];
    var clientHandle       = mClientHandle[aDeviceId];

    if (currentClientSeqId == null)
    {
        console.log("DeviceResponse, No client has registered yet: " +aSequenceId);        
        return;
    }

    // Is this the response to the current request and do we have waiting client - or
    // has the response already been sent to the client?
    //
    if (aSequenceId == currentClientSeqId)
    {
        if (clientHandle == null)
        {
            console.log("DeviceResponse, Response has already been sent to client: " +aSequenceId);
        }
        else
        {
            console.log("DeviceResponse, Forwarding response to client: " +aSequenceId);
            clientHandle.send(aPayload);            

            mClientHandle[aDeviceId] = null;
        }

        return;
    }

    // aSequenceId != currentClientSeqId

    // This could be a duplicate (old) response from the device for an old command and can be ignored.
    //
    // However, we can use the message to send the pending (new) request to the device.
    //
    console.log("DeviceResponse, Device is ready for a new request: " +aSequenceId);

    var clientRequestPayload = mClientRequest[aDeviceId];
    
    aResponse.send(clientRequestPayload);

    mDeviceHandle[aDeviceId] = null;
}

function makeCommand(aMethod, aSequenceId, aCommand)
{
    var commandObject =
    {
        sequenceId: aSequenceId,
        method:     aMethod,
        command:    aCommand
    };

    var commandString = JSON.stringify(commandObject);

    return commandString;
}
