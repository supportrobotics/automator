var express = require("express");
 var app = express();

 /* serves main page */
 app.get("/", function(aRequest, aResponse)
 {
     var path = __dirname + "/www/index.htm";

    aResponse.sendFile(path);
 });

  app.post("/user/add", function(aRequest, aResponse) { 
	/* some server side logic */
	aResponse.send("OK");
  });

 /* serves all the static files */
 app.get(/^\/device\/(.*)\/(.)\/$/, function(aRequest, aResponse)
 { 
     console.log('Path Request - Params: ' + JSON.stringify(aRequest.params));
     console.log('Path Url:    ' + aRequest.url);
     console.log('Path Method: ' + aRequest.method);    

     setTimeout(completeRequest, 2000, aResponse); 
     //aResponse.send("-- got abc --"); 
 });

 function completeRequest(aResponse)
 {
	aResponse.send("-- got --"); 
 }

 /* serves all the static files */
 app.get(/^(.+)$/, function(aRequest, aResponse)
 { 
     console.log('static file request : ' + JSON.stringify(aRequest.params));

     var path = __dirname + "/www/" + aRequest.params[0];

     console.log('static file path: ' + path);

     aResponse.sendFile(path); 
 });

 var port = process.env.PORT || 5000;

 app.listen(port, function()
 {
   	console.log("Listening on " + port);
 });


