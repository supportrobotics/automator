var CMDFLOWS = (function(){

var PAGE_RETRY_ATTEMPTS_LONG = CMDPLAYER.PAGE_RETRY_ATTEMPTS_LONG;

var BRIDGE = MMRSUTILS.getDeviceBridge();

function trace(aText) { MMRSUTILS.doTrace(exports.name, aText); }
function warn(aText)  { MMRSUTILS.doWarn (exports.name, aText); }
function error(aText) { MMRSUTILS.doError(exports.name, aText); }

var mFlows = {};

var SELF_CHECK = 
[
    { //
        pageId:  "mmrshome.html",
        action:  "click",
        element: "selfcheck"
    }
];

//=============================================================================
// FAULT SCREENS
//=============================================================================
//
var FAULT_CAMERA = 
[    
    { //
        pageId:  "page-device-test-select",
        action:  "click",
        element: "itemTestSelector4",   
        comment: "Choose Camera and Flash"
    }, 
    { // 
        pageId:  "page-device-test-select",
        action:  "click",
        element: "continueButton"
    },         
    { // 
        pageId:  "CameraFrontViewfinderTest",
        action:  "click",
        element: "continueButton"
    },   
    { //
        pageId:  "CameraFrontViewfinderTest",            
        action:  "click",
        element: "positiveButton"
    },
    { // 
        pageId:  "CameraRearViewfinderTest",
        action:  "click",
        element: "continueButton"
    },   
    { // 
        pageId:  "CameraRearViewfinderTest",            
        action:  "click",
        element: "negativeButton"
    },
    { // 
        pageId:  "CameraFlashTest",
        action:  "click",
        element: "continueButton"
    },   
    { // 
        pageId:  "CameraFlashTest",            
        action:  "click",
        element: "negativeButton"
    }, 
    { // 
        pageId:  "e2edevicetest.html",            
        action:  "click",
        element: "continueButton"
    }
];                

//=============================================================================
// DAMAGE SCREENS
//=============================================================================
//
var DAMAGE_NONE = // DAMAGE_NONE
[
    { //
        pageId:  "page-test-results-negative",
        action:  "click",
        element: "continueButton"
    },         
    { // 
        pageId:  "page-screen-check",            
        action:  "click",
        element: "itemCarrouselChoice0"
    },
    { // 
        pageId:  "page-screen-check",
        action:  "click",
        element: "continueButton"
    },        
    { // 
        pageId:  "page-case-check",            
        action:  "click",
        element: "itemCarrouselChoice0"
    },
    { // 
        pageId:  "page-case-check",
        action:  "click",
        element: "continueButton"
    },         
    { // 
        pageId:  "page-liquid-check",            
        action:  "click",
        element: "itemCarrouselChoice0"
    },
    { // 
        pageId:  "page-liquid-check",
        action:  "click",
        element: "continueButton"
    }
]; 

var DAMAGE_LIQUID = // DAMAGE_LIQUID
[
    { //
        pageId:  "page-test-results-negative",
        action:  "click",
        element: "continueButton"
    },         
    { // 
        pageId:  "page-screen-check",            
        action:  "click",
        element: "itemCarrouselChoice0"
    },
    { // 
        pageId:  "page-screen-check",
        action:  "click",
        element: "continueButton"
    },        
    { // 
        pageId:  "page-case-check",            
        action:  "click",
        element: "itemCarrouselChoice0"
    },
    { // 
        pageId:  "page-case-check",
        action:  "click",
        element: "continueButton"
    },         
    { // 
        pageId:  "page-liquid-check",            
        action:  "click",
        element: "itemCarrouselChoice1"
    },
    { // 
        pageId:  "page-liquid-check",
        action:  "click",
        element: "continueButton"
    }
];

//=============================================================================
// TEST VALUE SCREEN
//=============================================================================
//

var TEST_VALUE_DEVICE_HTCU11 =
[
    { // 18
        pageId:  "page-mock-values",
        action:  "select",
        element: "imei",
        option:  "-- Random HTCU11:",
        offset:  1,
        pause:   3
    }
];  

var TEST_VALUE_WARRANTY_EXPIRED = 
[
    { // Select out of warranty
        pageId:  "page-mock-values",
        action:  "select",
        element: "purchaseDate",
        option:  "-- Out Of Warranty Period:",
        offset:  1
    }
];

var TEST_VALUE_WARRANTY_PERIOD = 
[
    { // Select out of warranty
        pageId:  "page-mock-values",
        action:  "select",
        element: "purchaseDate",
        option:  "-- Within Warranty Period:",
        offset:  1
    }
];

var TEST_VALUE_STOCK_NONE = 
[
    { // Select out of warranty
        pageId:  "page-mock-values",
        action:  "select",
        element: "stockLevel",
        option:  "0"
    }
];

var TEST_VALUES_CONTINUE = 
[
    { // Leave the test values page
        pageId:  "page-mock-values",
        action:  "click",
        element: "continueButton",
        pause:   5
    }
];

var DEVICE_ASSESSMENT_CONTINUE =
[
    { // Device Assessment
        pageId:  "page-decision",
        action:  "click",
        element: "continueButton",
        pause:   5
    }
];

//=============================================================================
// COST ACKNOWLEDGEMENT SCREENS
//=============================================================================
//

var FAULT_NO_DAMAGE_NO_WARRANTY_COST_ACK = 
[
    { // Out of Warranty, Cost acknowledge
        pageId:  "page-fault-no-damage-no-warranty",
        action:  "click",
        element: "continueButton"
    }
];

var FAULT_NO_DAMAGE_WARRANTY_COST_ACK = 
[
    { 
        pageId:  "page-fault-no-damage-warranty",
        action:  "click",
        element: "continueButton"
    }
];


var FAULT_DAMAGE_WARRANTY_COST_ACK = 
[
    { 
        pageId:  "page-fault-damage-warranty",
        action:  "click",
        element: "continueButton"
    }
];

//=============================================================================
// LOGIN SCREENS
//=============================================================================
//

var LOGIN_TDID =
[
    { // Login
        pageId:  ".*-login",        
        action:  "click",
        element: "loginSubmit"
    }
];

//=============================================================================
// TERMS & CONDITIONS SCREENS
//=============================================================================
//

var ACCEPT_TCS =
[     
    { // Accept T&Cs
        pageId:  ".*-ts-and-cs",
        action:  "click",
        element: "continueButton",
        retries: PAGE_RETRY_ATTEMPTS_LONG
    }
];

//=============================================================================
// ADDRESS SCREENS
//=============================================================================
//
var ADDRESS_CHANGE = 
[
    { // Select the change address link
        pageId:  ".*-address",            
        action:  "click",
        element: "changeAddressLink"
    }
];

var ADDRESS_EDIT =
[    
    { // Enter part of the address
        pageId:  ".*-address-edit",            
        action:  "set",
        element: "addressInput",
        value:    "242 exhibition",
        pause:    5
    },
    { // Choose first address
        pageId:  ".*-address-edit",                        
        action:  "click",
        element: "itemAddressSelect0",
        retries:  3,         
        pause:    5
    },  
    { // Confirm address
        pageId:  ".*-address-edit",
        action:  "click",
        element: "continueButton",
        retries:  3        
    }
];

var ADDRESS_USE =
[
    { // Accept T&Cs
        pageId:  ".*-address",
        action:  "click",
        element: "continueButton"
    }
];

//=============================================================================
// SATCHEL SCREENS
//=============================================================================
//
var SATCHEL_REQUEST_REPLACE = 
[
    { // Hit RR Submit
        pageId:  "page-fault-no-damage-warranty-stock",
        action:  "click",
        element: "toggle-0"
    },
    { // Hit RR Submit
        pageId:  "page-fault-no-damage-warranty-stock",
        action:  "click",
        element: "doReplace"
    }    
];

var SATCHEL_REQUEST_REPAIR = 
[
    { // Hit RR Submit
        pageId:  "page-fault-no-damage-warranty-no-stock",
        action:  "click",
        element: "toggle-0"
    },
    { // Hit RR Submit
        pageId:  "page-fault-no-damage-warranty-no-stock",
        action:  "click",
        element: "doRepair"
    }    
];

//=============================================================================
// CONFIRMATION SCREENS
//=============================================================================
//
var FAULT_NO_DAMAGE_NO_WARRANTY_SUBMIT = 
[
    { // Hit RR Submit
        pageId:  "page-fault-no-damage-no-warranty-confirmation",
        action:  "click",
        element: "continueButton",
        pause:   10
    }
];

var FAULT_NO_DAMAGE_WARRANTY_STOCK_SUBMIT = 
[
    { // Hit RR Submit
        pageId:  "page-fault-no-damage-warranty-stock-confirmation",
        action:  "click",
        element: "continueButton",
        pause:   10
    }
];

var FAULT_NO_DAMAGE_WARRANTY_NO_STOCK_SUBMIT = 
[
    { // Hit RR Submit
        pageId:  "page-fault-no-damage-warranty-no-stock-confirmation",
        action:  "click",
        element: "continueButton",
        pause:   10        
    }
];

var FAULT_DAMAGE_SUBMIT = 
[
    { // Hit RR Submit
        pageId:  "page-fault-damage-confirmation",
        action:  "click",
        element: "continueButton",
        pause:   10        
    }
];

//=============================================================================
// RR CREATE SCREENS
//=============================================================================
//

var REPAIR_CREATE_FAILED = 
[
    { // 
        pageId:  "page-rr-repair-submit",
        action:  "isvisible",
        element: "cancelButton",
        retries:  1
    }
];

var REPAIR_CREATE_SUCCESS = 
[
    { // 
        pageId:  "page-rr-repair-submit",
        action:  "isvisible",
        element: "successButton",
        retries:  1
    }
];

//=============================================================================
// FLOWS
//=============================================================================
//
mFlows.FAULT_NO_DAMAGE_NO_WARRANTY =
{
    expectedOutput : "data/fault-no-damage-no-warranty.json",

    commands:
    [
        //SELF_CHECK,
        //FAULT_CAMERA,
        //DAMAGE_NONE,
        //TEST_VALUE_DEVICE_HTCU11,
        //TEST_VALUE_WARRANTY_EXPIRED,
        //TEST_VALUES_CONTINUE,
        //DEVICE_ASSESSMENT_CONTINUE,
        //FAULT_NO_DAMAGE_NO_WARRANTY_COST_ACK,
        //LOGIN_TDID,
        //ACCEPT_TCS,
        //ADDRESS_CHANGE,
        //ADDRESS_EDIT,
        //ADDRESS_USE,
        FAULT_NO_DAMAGE_NO_WARRANTY_SUBMIT
    ]
};


mFlows.FAULT_NO_DAMAGE_WARRANTY_STOCK =
{
    expectedOutput : "data/fault-no-damage-warranty-stock.json",

    commands:
    [
        SELF_CHECK,
        FAULT_CAMERA,
        DAMAGE_NONE,
        TEST_VALUE_DEVICE_HTCU11,
        TEST_VALUE_WARRANTY_PERIOD,
        TEST_VALUES_CONTINUE,
        DEVICE_ASSESSMENT_CONTINUE,
        SATCHEL_REQUEST_REPLACE,
        LOGIN_TDID,
        ACCEPT_TCS,
        ADDRESS_CHANGE,
        ADDRESS_EDIT,
        ADDRESS_USE, 
        FAULT_NO_DAMAGE_WARRANTY_STOCK_SUBMIT
    ]
};   

mFlows.FAULT_NO_DAMAGE_WARRANTY_NO_STOCK =
{
    expectedOutput : "data/fault-no-damage-warranty-no-stock.json",

    commands:
    [
        SELF_CHECK,
        FAULT_CAMERA,
        DAMAGE_NONE,
        TEST_VALUE_DEVICE_HTCU11,
        TEST_VALUE_WARRANTY_PERIOD,
        TEST_VALUE_STOCK_NONE,
        TEST_VALUES_CONTINUE,
        DEVICE_ASSESSMENT_CONTINUE,
        SATCHEL_REQUEST_REPAIR,
        LOGIN_TDID,
        ACCEPT_TCS,
        ADDRESS_CHANGE,
        ADDRESS_EDIT,
        ADDRESS_USE, 
        FAULT_NO_DAMAGE_WARRANTY_NO_STOCK_SUBMIT
    ]
};

mFlows.FAULT_DAMAGE_WARRANTY =
{
    expectedOutput : "data/fault-damage-warranty.json",

    commands:
    [
        SELF_CHECK,
        FAULT_CAMERA,
        DAMAGE_LIQUID,
        TEST_VALUE_DEVICE_HTCU11,
        TEST_VALUE_WARRANTY_PERIOD,
        TEST_VALUES_CONTINUE,
        DEVICE_ASSESSMENT_CONTINUE,
        FAULT_DAMAGE_WARRANTY_COST_ACK,    
        /* No satchel request in this flow - is that correct? */
        LOGIN_TDID,
        ACCEPT_TCS,
        ADDRESS_CHANGE,
        ADDRESS_EDIT,
        ADDRESS_USE, 
        FAULT_DAMAGE_SUBMIT,
        REPAIR_CREATE_SUCCESS
    ]
}; 



//===================================================================================================
// MAIN
//===================================================================================================
//


function expandFlow(aInputFlow, aOutputFlow)
{
    for (var index in aInputFlow)
    {
        var thisItem = aInputFlow[index];

        if (thisItem == null)
        {
            warn("expandFlow, null item in flow: " +index);
            continue;
        }

        if (Array.isArray(thisItem))
        {
            expandFlow(thisItem, aOutputFlow);
            continue;
        }

        aOutputFlow.push(thisItem);
    }
}

var exports = {};

function exportFlows()
{
    for (var index in mFlows)
    {
        var thisFlow = mFlows[index];

        var expandedCmds = [];

        expandFlow(thisFlow.commands, expandedCmds);

        thisFlow.commands = expandedCmds;

        exports[index] = thisFlow;
    }
}

{
    exportFlows(mFlows);
}


//===================================================================================================
// Exports
//===================================================================================================
//


exports.name = "cmdplayer";

exports.PAGE_RETRY_ATTEMPTS_LONG = PAGE_RETRY_ATTEMPTS_LONG;


//exports.playCommands = function(aCommands, aIndex, aCallback) { return playCommands(aCommands, aIndex, aCallback); }


return exports;

}());
