var MMRSHOME = (function() {
	"use strict"

var mIsNativeRun = false; //MMRSUTILS.isNativeRun();

var BRIDGE = MMRSUTILS.getDeviceBridge();

function trace(aText) { MMRSUTILS.doTrace(exports.name, aText); }
function warn(aText)  { MMRSUTILS.doWarn (exports.name, aText); }
function error(aText) { MMRSUTILS.doError(exports.name, aText); }

var EOR = String.fromCharCode(10);

window.onerror = function(aMessage) { error(aMessage); }

var mTheApp = angular.module('aApp', []);

mTheApp.controller('aController', function($scope, $timeout)
{
    var mRequestHostUrl = BRIDGE.getRequestHostUrl() + "/" +BRIDGE.getParam("deviceDir") + "/";

    //var SOURCE_URL = "";

    var SOURCE_URL = mRequestHostUrl+ "webview/";

    $scope.menuItems =
    [
        {
            action:  "click",
            element: "continueButton",
            icon:    "images/iconoptimise.png",
            title:   "Continue",
            comment: "Press continue button"
        },    
        {
            action:  "click",
            element: "selfcheck",
            icon:    "images/icontransfer.png",
            title:   "Self Care",
            comment: "Select self care",
        },    
        {
            action:  "click",
            element: "itemTestSelector4",   
            icon:    "images/iconoptimise.png",
            title:   "Camera and Flash",
            comment: "Choose option"
        },
        {
            action:  "click",
            element: "positiveButton",
            icon:    "images/iconoptimise.png",
            title:   "Pass Test",
            comment: "Press pass button"
        },
        { 
            action:  "click",
            element: "negativeButton",
            icon:    "images/iconoptimise.png",
            title:   "Fail Test",
            comment: "Press fail button"
        },

        {
            action:  "click",
            element: "itemCarrouselChoice0",
            icon:    "images/iconoptimise.png",
            title:   "First Choice",
            comment: "Select first carrousel item"
        },
        {
            action:  "click",
            element: "next",
            icon:    "images/iconoptimise.png",
            title:   "Carrousel Continue",
            comment: "Select next test"
        },
        {
            action:  "click",
            element: "loginSubmit",
            icon:    "images/iconoptimise.png",
            title:   "Log in",
            comment: "Select Log in"
        },
        {
            action:  "click",
            element: "changeAddressLink",
            icon:    "images/iconoptimise.png",
            title:   "Change Address",
            comment: "Select change address link"
        },
        {
            action:  "set",
            element: "addressInput",
            value:    "242 exhibition",
            icon:    "images/iconoptimise.png",
            title:   "Enter Address",
            comment: "Enter '242 exhibition'"
        },
        {
            action:  "click",
            element: "itemAddressSelect0",
            icon:    "images/iconoptimise.png",
            title:   "Use first address",
            comment: "Select first address in list"
        },        
        {
            action:  "click",
            element: "imei",
            icon:    "images/iconoptimise.png",
            title:   "IMEI",
            comment: "Select IMEI Drop Down"
        },         
        {
            action:  "select",
            element: "imei",
            value:   "7",
            icon:    "images/iconoptimise.png",
            title:   "7th IMEI Value",
            comment: "Select IMEI in list"
        },          
        {
            action:  "select",
            element: "legalStatus",
            value:   "1",
            icon:    "images/iconoptimise.png",
            title:   "Full Authority",
            comment: "Select Legal Status - Full Authority "
        },         

    ];

    $scope.onMenuItemClick = function(aItem)
    {
        trace("onMenuItemClick, Item: " +aItem.title);

        var command = {};

        command.action  = aItem.action;
        command.element = aItem.element;
        command.value   = aItem.value;

        var commandStr = JSON.stringify(command);

        MMRSUTILS.sendPostRequest("http://localhost:31313/", "123", null, commandStr, onPostResponse);
   
        //window.location.href = aItem.action;
    }

    function onPostResponse(aRequestId, aSuccess, aResponse)
    {
        trace("onPostResponse, ID: " +aRequestId+ ", Success: " +aSuccess+ ", Response: " +aResponse);
    }

    function getTimeNow()
    {
        var dateNow = new Date();
        var timeNow = dateNow.getTime();

        return timeNow;
    }

    ///////////////////////////////////////////////////////////////////////////////////////

 	$scope.doLoad = function()
    {
	    //trace(">> doLoad");

	    //trace("<< doLoad");
	}

    $scope.doLoad();



});

function onLoad()
{
}

var exports = {};

exports.name = "mmrshome";

exports.onLoad = function(aOptions) { onLoad(aOptions); }

return exports;

}());
