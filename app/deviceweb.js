var DEVICEWEB = (function() {

var REQUEST_HOST_URL = "device/";

var mHttpPageRequest   = null;
var mActivePageRequest = null;
var mRequestQueue      = [];

var mHeaderEnd = "" + String.fromCharCode(13) + String.fromCharCode(10);

var HTTP_TIMEOUT = 5*1000;

function trace(aText) { MMRSUTILS.doTrace(exports.name, aText); }
function note(aText)  { MMRSUTILS.doTrace(exports.name, aText); }
function warn(aText)  { MMRSUTILS.doWarn (exports.name, aText); }
function error(aText) { MMRSUTILS.doError(exports.name, aText); }

function showNetworkActive(aActive)
{
}

function showSpinner(aActive)
{
    trace("showSpinner, Active: " +aActive);
}

function showOkDialog(aTitle, aMessage)
{
    var title   = aTitle   == null ? "" : aTitle;
    var message = aMessage == null ? "" : aMessage;

    window.alert(title+ "\n\n" +message);
}

function setAppState(aStatus)
{
    trace("setAppState, Status: " +aStatus);  
}

function getRequestHostUrl()
{
    return REQUEST_HOST_URL;
}

function isPageCached(aPage)
{
    var cached = localStorage.getItem(aPage) != null;
    return cached;
}

function getValue(aKey)
{
    var data = localStorage.getItem(aKey);
    //trace("getValue, Key: " +aKey+ ", Value: " +data);
    return data;
}

function setValue(aKey, aValue)
{
    //trace("setValue, Key: " +aKey+ ", Value: " +aValue);
    localStorage.setItem(aKey, aValue);
}

function getParam(aKey)
{
    return "";  
}

function readPage(aPage)
{
    var data = localStorage.getItem(aPage);
    return data;
}

function writePage(aPage, aData)
{
    localStorage.setItem(aPage, aData);
}

function deletePage(aPage)
{
    localStorage.setItem(aPage, null);
}

function splitResponse(aText)
{
    var bodyText = null;

    if (aText == null)
    {
        return [null, null];
    }

    if (aText == "")
    {
        return [null, ""];
    }

    var headerEnd = aText.indexOf(mHeaderEnd);

    if (headerEnd == -1)
    {
        //trace("splitResponse, Response has no header/body divider");
        return [null, aText];        
    }

    var bodyHeader = aText.substring(0, headerEnd);

    bodyText = aText.substr(headerEnd+4);

    //trace("splitResponse, Header: " +bodyHeader);
    //trace("splitResponse, Body: "   +bodyText);

    return [bodyHeader, bodyText];
}

function abortUrlRequest()
{
    if (mHttpPageRequest != null)
    {
        mHttpPageRequest.abort();
        //mHttpPageRequest = null;
    }
}

function sendUrlRequest(aRequest)
{
    //trace("sendUrlRequest, Request: " +aRequest.url);   

    if (window.XMLHttpRequest)
    {
        mHttpPageRequest = new XMLHttpRequest();
    }
    else
    {
        mHttpPageRequest = new ActiveXObject("Microsoft.XMLHTTP");
    }

    mHttpPageRequest.onreadystatechange = function()
    {
        //trace("sendUrlRequest, onreadystatechange - Enter"); 
        
        if (mHttpPageRequest.readyState != 4)
        {
            //trace("sendUrlRequest, onreadystatechange - Not Ready"); 
            return;
        }
    
        //trace("sendUrlRequest, onreadystatechange - Ready"); 
        
        if (mHttpPageRequest.status != 200)
        {
            trace("sendUrlRequest, onreadystatechange - Unable to Get Page, Status: " +mHttpPageRequest.status);

            aRequest.callback(aRequest.url, false, null, null);
            requestNextUrl();
            return;
        }

        //var contentType = mHttpPageRequest.getResponseHeader("Content-Type");

        var responseHeader = mHttpPageRequest.getAllResponseHeaders();
        var responseBody   = mHttpPageRequest.responseText;

        //trace("sendUrlRequest, HTTP Headers: " +responseHeader);

        if (responseBody == null)
        {
            //trace("sendUrlRequest, onreadystatechange - Unable to get response body");
            aRequest.callback(aRequest.url, false, responseHeader, null);
            requestNextUrl();            
            return;
        }

        //trace("sendUrlRequest, onreadystatechange - Response Length: " + responseBody.length);

        var response = splitResponse(responseBody);

        var bodyHeader = response[0];
        var bodyText   = response[1];

        if (bodyHeader == null)
        {
            bodyHeader = responseHeader;
        }

        aRequest.callback(aRequest.url, true, bodyHeader, bodyText);
        mActivePageRequest = null;
        requestNextUrl();
    }

    //trace("sendUrlRequest, Sending request: " +aRequest.url);   
    
    mHttpPageRequest.open("GET", aRequest.url, true);

    mHttpPageRequest.timeout = HTTP_TIMEOUT;

    //if (aRequest.headers == null)
    {
        mHttpPageRequest.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    }
    //else
    {
        var headers = aRequest.headers;

        for (var key in headers)
        {
            var value = headers[key];
            mHttpPageRequest.setRequestHeader(key, value);
        }
    }


    mActivePageRequest = aRequest;

    mHttpPageRequest.send();
}

function getUrl(aUrl, aHeaders, aCallback)
{
    trace("getUrl, Url:" +aUrl);   

    var queryPos = aUrl.indexOf('?');

    var concatChar = queryPos == -1 ? "?" : "&";

    var dateNow = new Date();
    var timeNow = dateNow.getTime();

    aUrl += concatChar+ "time="  +timeNow;    

    request = {};
    request.url      = aUrl;
    request.callback = aCallback;
    request.headers  = aHeaders;

    if (mActivePageRequest != null)
    {    
        //trace("getUrl, Request Active, Queing: " +aUrl); 
              
        mRequestQueue.push(request);
        return;
    }
    
    sendUrlRequest(request);

    return "";
}


function requestNextUrl()
{
    if (mRequestQueue.length == 0)
    {
        //trace("requestNextUrl, Queue Empty - No more pages to fetch");   
        return;
    }
    
    request = mRequestQueue.shift();
    
    //trace("requestNextUrl, Requesting next page");   

    sendUrlRequest(request);
}

var mPostRequestCount = 0;

function postUrl(aUrl, aHeaders, aBody, aCallback)
{
    trace("postUrl, Url: " +aUrl);

    mPostRequestCount++;

    var requestId = "HTTP-POST-RQST-" +mPostRequestCount;

    MMRSUTILS.sendPostRequest(aUrl, requestId, aHeaders, aBody,
        function (aRequestId, aSuccess, aResponse)
        {
            aCallback(aUrl, aSuccess, null, aResponse);
        }
    );  

    return requestId;  
}

function setHeaderStyle(aShowStatusBar, aShowNavBar)
{
    trace("setHeaderStyle, Status Bar: " +aShowStatusBar+  ", Nav Bar: " +aShowNavBar);   
}

function sendSetCommand(aUrl, aPayload, aCallback)
{
    trace("sendSetCommand, Url: " +aUrl+ ", Payload: " +aPayload);

    setTimeout(aCallback, 100, "CMD-123", 1, "{}");
}

function popPage()
{
    trace("popPage");
    window.history.back();    
}

function popToHomePage()
{
    trace("popToHomePage");
}

function setPageBackground(aBackground)
{
    trace("setPageBackground, Background: " +aBackground);  

    var color = aBackground - 0; 

    trace("setPageBackground, Color: " +color);  

    var red   = Math.floor(color / (256 * 256)) % 256;

    var green = Math.floor(color / 256) % 256;

    var blue  = color % 256;

    var rgb  = "rgb(" +red+ "," +green+ "," +blue+ ")";

    trace("setPageBackground, RGB: " +rgb);  

    document.body.style.backgroundColor = rgb;
}

function convertToUTCTime(aDate, aFormat, aTimeZone)
{
    var time = Date.parse(aDate);

    time = Math.floor(time/1000);

    return time;    
}


//=============================================================================
// Exports
//=============================================================================

var exports = {};

exports.name = "deviceweb";

exports.convertToUTCTime  = function(aDate, aFormat, aTimeZone) { return convertToUTCTime(aDate, aFormat, aTimeZone); }

exports.popPage = function() { return popPage(); }

exports.popToHomePage = function() { return popToHomePage(); }

exports.setPageBackground = function(aBackground) { return setPageBackground(aBackground); }

exports.showSpinner  = function(aActive)            { return showSpinner(aActive); }
exports.showOkDialog = function(aTitle, aMessage)          { return showOkDialog(aTitle, aMessage); }

exports.getRequestHostUrl = function()             { return getRequestHostUrl(); }

exports.getValue       = function(aKey)            { return getValue(aKey); }
exports.setValue       = function(aKey, aValue)    { return setValue(aKey, aValue); }

exports.getParam       = function(aKey)            { return getParam(aKey); }

exports.readPage       = function(aPage)           { return readPage(aPage); }
exports.writePage      = function(aPage, aData)    { return writePage(aPage, aData); }
exports.deletePage     = function(aPage)           { return deletePage(aPage); }

exports.fetchUrl        = function(aUrl, aCallback, aHeaders)          { return getUrl(aUrl, aHeaders, aCallback); }

exports.getUrl          = function(aUrl, aHeaders, aCallback)          { return getUrl(aUrl, aHeaders, aCallback); }
exports.postUrl         = function(aUrl, aHeaders, aBody, aCallback)   { return postUrl(aUrl, aHeaders, aBody, aCallback); }

exports.getCellularUrl  = function(aUrl, aHeaders, aCallback)          { return getUrl(aUrl, aHeaders, aCallback); }
exports.postCellularUrl = function(aUrl, aHeaders, aBody, aCallback)   { return postUrl(aUrl, aHeaders, aBody, aCallback); }

exports.setAppState = function(aStatus) { return setAppState(aStatus); }

exports.setHeaderStyle   = function(aShowStatusBar, aShowNavBar) { return setHeaderStyle(aShowStatusBar, aShowNavBar); }

exports.sendSetCommand   = function(aUrl, aPayload, aCallback) { return sendSetCommand(aUrl, aPayload, aCallback); }

return exports;

}());

