var MMRSHOME = (function() {
	"use strict"

var mIsNativeRun = false; //MMRSUTILS.isNativeRun();

var BRIDGE = MMRSUTILS.getDeviceBridge();

function trace(aText) { MMRSUTILS.doTrace(exports.name, aText); }
function warn(aText)  { MMRSUTILS.doWarn (exports.name, aText); }
function error(aText) { MMRSUTILS.doError(exports.name, aText); }

var EOR = String.fromCharCode(10);

window.onerror = function(aMessage) { error(aMessage); }

var mTheApp = angular.module('aApp', []);

mTheApp.controller('aController', function($scope, $timeout)
{
    var mRequestHostUrl = BRIDGE.getRequestHostUrl() + "/" +BRIDGE.getParam("deviceDir") + "/";

    //var SOURCE_URL = "";

    var SOURCE_URL = mRequestHostUrl+ "webview/";

    $scope.menuItems =
    [
        { // 0
            id:      "transfer",
            action:  "post",
            icon:    "images/icontransfer.png",
            title:   "Post Message",
            comment: "Manage your content",
        },    
        { // 1
            action:  "localcheck.html",
            icon:    "images/iconoptimise.png",
            title:   "Self-Care",
            comment: "Optimise & test your device"
        },
        { // 2
            action:  "remotecheck.html",
            icon:    "images/iconagent.png",
            title:   "Assisted care",
            comment: "Get support from a Telstra agent"
        },
        { // 3
            action:  "hardwaretests.html",
            hide:    true,
            icon:    "images/iconoptimise.png",
            title:   "Hardware Tests",
            comment: "Internal - Hardware diagnostics"
        },        
        { // 4
            action:  "doswap",
            hide:    true,
            icon:    "images/iconplan.png",
            title:   "Your lease plan",
            comment: "Your plan and upgrade options"
        }

    ];

    $scope.onMenuItemClick = function(aItem)
    {
        trace("onMenuItemClick, Item: " +aItem.title);

        if (aItem.action == "post")
        {
            MMRSUTILS.sendPostRequest("http://localhost:31313/fred", "123", null, "Hello\nWorld", onPostResponse);
            return;
        }

        //window.location.href = aItem.action;
    }

    function onPostResponse(aRequestId, aSuccess, aResponse)
    {
        trace("onPostResponse, ID: " +aRequestId+ ", Success: " +aSuccess+ ", Response: " +aResponse);
    }

    function getTimeNow()
    {
        var dateNow = new Date();
        var timeNow = dateNow.getTime();

        return timeNow;
    }

    ///////////////////////////////////////////////////////////////////////////////////////

 	$scope.doLoad = function()
    {
	    //trace(">> doLoad");

	    //trace("<< doLoad");
	}

    $scope.doLoad();



});

function onLoad()
{
}

var exports = {};

exports.name = "mmrshome";

exports.onLoad = function(aOptions) { onLoad(aOptions); }

return exports;

}());
