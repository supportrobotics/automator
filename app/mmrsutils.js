var MMRSUTILS = (function(){

var mIsAndroid = (navigator.userAgent.indexOf("Android") > 0) ||
                 (navigator.userAgent.indexOf("android") > 0);

var mIsIPhone  = (navigator.userAgent.indexOf("iPhone")  > 0) ||
                 (navigator.userAgent.indexOf("iPad")    > 0);      

function doTrace(aTag, aText)
{
    if (mIsAndroid && window.NATIVE)
    {
        NATIVE.itrace(aTag+ "::" +aText);
        return;
    }

    if (console.log)
    {
       console.log(aTag+ "::" +aText);
    }
}

function doWarn(aTag, aText)
{
    if (mIsAndroid && window.NATIVE)
    {
        NATIVE.iwarn(aTag+ "::" +aText);
        return;
    }

    if (console.warn)
    {
        console.warn(aTag+ "::" +aText);
        return;        
    }

    if (console.log)
    {
        console.log("+++ " +aTag+ "::" +aText);        
    }
}

function doError(aTag, aText)
{
    if (mIsAndroid && window.NATIVE)
    {
        NATIVE.ierror(aTag+ "::" +aText);
        return;
    }

    if (console.error)
    {
        console.error(aTag+ "::" +aText);
        return;        
    }

    if (console.log)
    {
        console.log("*** " +aTag+ "::" +aText);        
    }
}

function trace(aText) { doTrace(exports.name, aText); }
function warn(aText)  { doWarn (exports.name, aText); }
function error(aText) { doError(exports.name, aText); }

function isAndroidRun()
{
    return mIsAndroid;
}

function isIPhoneRun()
{
    return mIsIPhone;
}

function isNativeRun()
{
    var isNative = mIsAndroid || mIsIPhone;

    return isNative;
}

var MMRSDEVICEWEB = {};

function getDeviceBridge()
{
    if (mIsAndroid)
    {
        return MMRSNATIVE;
    }

    if (mIsIPhone)
    {
        return IOSNATIVE;
    }

    if (DEVICEWEB != null)
    {
        return DEVICEWEB;
    }

    return MMRSDEVICEWEB;
}

//===================================================================================================
// Object Utilities
//===================================================================================================
//
function copyData(aObject)
{
    var copy = null;

    try
    {
        copy = JSON.parse(JSON.stringify(aObject));
    }
    catch (err)
    {
        error("copyData, Exception: " +err);
        copy = null;
    }

    return copy;
}

function contains(aLine, aWord)
{
    if (aLine == null || aWord == null)
    {
        return false;
    }

    var line = aLine.toLowerCase();
    var word = aWord.toLowerCase();

    var wordPos = line.indexOf(word);

    return wordPos != -1;
}


function setElement(aItemId, aContents)
{
    var item = document.getElementById(aItemId);

    if (item == null)
    {
        error("setElement, Cannot find element: " +aItemId);
        return;
    }

    item.innerHTML = aContents;
}

function getElementPosition(aElementId)
{
    var viewPortoffsetY = window.pageYOffset;
    var viewPortoffsetX = window.pageXOffset;

    //trace("getElementPosition, View Port X: " +viewPortoffsetX+ ", Y: " +viewPortoffsetY);

    var area = document.getElementById(aElementId);

    if (area == null)
    {
    	error("getElementPosition, Cannot find: " +aElementId);
        return null;
    }

    var rect = area.getBoundingClientRect();

    var offsetY = viewPortoffsetY + rect.top; // Add on the scroll
    var offsetX = viewPortoffsetX + rect.left;

    var width  = rect.right  - rect.left;
    var height = rect.bottom - rect.top;

    var point = {x: offsetX, y: offsetY, w: width, h: height};

    return point;
}


function getScreenDimensions()
{
    var width  = window.innerWidth
            || document.documentElement.clientWidth
            || document.body.clientWidth;

    var height = window.innerHeight
            || document.documentElement.clientHeight
            || document.body.clientHeight;

    trace("getScreenDimensions, Width: " +width+ ", Height: " +height);
 
    var screenSize = { w: width, h: height};

    return screenSize;
}


function getAngularScope(aAngularArea)
{
//--    trace(">> getAngularScope");

    var angularElement = document.getElementById(aAngularArea);

    if (angularElement == null)
    {
        error("getAngularScope, Cannot find angular area element: " +aAngularArea);
        return null;
    }

    var scope = angular.element(angularElement).scope();

    if (scope == null)
    {
        error("getAngularScope, cannot get angular scope in area: " +aAngularArea);
        return null;
    }

//--    trace("<< getAngularScope");    

    return scope;
}

function getFieldValue()
{
    if (arguments.length < 2)
    {
        error("getFieldValue, Insufficient function arguments");
        return null;
    }

    var lastFieldIndex = arguments.length - 1;

    var record = arguments[0];

    var value = null;

    for (var index = 1; index < arguments.length; index++)
    {
        if (record == null)
        {
            break;
        }

        var thisField = arguments[index];

        var thisValue = record[thisField];

        if (index == lastFieldIndex)
        {
            value = thisValue;
            break;
        }

        record = thisValue;
    }

    return value;
}

function getOptions()
{
    if (isNativeRun())
    {
        var deviceBridge = getDeviceBridge();

        locationStr = deviceBridge.getPageQuery();
    }
    else
    {
        locationStr = new String(document.location);
    }


    var options = {};

    // Ignore any fragments
//--    trace("getOptions, aUrl: " + aUrl);
    var mainUrlAndFragment = locationStr.split('#');
    var locationStr = mainUrlAndFragment[0];

//--    trace("getOptions, Url: " + locationStr);

    var queryPos = locationStr.indexOf('?');

    if (queryPos == -1)
    {
        return options;
    }

    var queryStr = locationStr.slice(queryPos+1);

    var queries = queryStr.split('&');

//--    trace("getOptions, Queries: +" +queryStr+ "+");

    for (var index in queries)
    {
        var thisQuery = queries[index];

        var equalPos = thisQuery.indexOf("=");

        if (equalPos == -1)
        {
            options[thisQuery] = true;
        }
        else
        {
            var key   = thisQuery.substr(0, equalPos);
            var value = thisQuery.substr(equalPos+1);

            //trace("getOptions, Query: " +thisQuery+ ", Key: " +key+ ", Value: " +value+ ",");
            options[key] = value;
        }

//--        trace("getOptions, Key: +" +key+ "+ Value: +" + options[key] + "+");
    }

    return options;
}

function getUrlPath()
{
    //trace("getUrlPath");

    var locationStr  = null;

    if (isNativeRun())
    {
        var deviceBridge = getDeviceBridge();

        locationStr = deviceBridge.getPageUrl();
    }
    else
    {
        locationStr = new String(document.location);
    }


    trace("getUrlPath, Location: " + locationStr);

    // Ignore any fragments

    var mainUrlAndFragment = locationStr.split('#');

    var locationStr = mainUrlAndFragment[0];

    trace("getUrlPath, Url: " + locationStr);

    var queryPos = locationStr.indexOf('?');

    trace("getUrlPath, ? Pos: " + queryPos);

    var path = locationStr;        

    if (queryPos != -1)
    {
        path = locationStr.substring(0, queryPos);
    }

    trace("getHTMLPath, Path: " + path);

    
    return path;
}

function getRequestUrl(aHostUrl, aUrl)
{
    if (aUrl == null)
    {
        warn("getRequestUrl, Url is null");            
        return null;
    }        

    if (aUrl.substr(0, 4) == "http")
    {
        return aUrl;
    }

    var relativeUrl = aHostUrl + aUrl;

    return relativeUrl;
}


function mkStateChangeFn(aHttpRequest, aRequestId, aCallback)
{
    return function()
    {
//--        trace("stateChange, onreadystatechange - Enter (" +aRequestId+ ")"); 
        
        if (aHttpRequest.readyState != 4)
        {
//--            trace("stateChange, onreadystatechange - Not Ready (" +aRequestId+ ")"); 
            return;
        }
    
//--        trace("stateChange, onreadystatechange - Ready (" +aRequestId+ ")"); 
        
        if (aHttpRequest.status == 200)
        {
            var contentType = aHttpRequest.getResponseHeader("Content-Type");
            
//--            trace("stateChange, onreadystatechange - Content Type: " +contentType+ " (" +aRequestId+ ")");

            var text = aHttpRequest.responseText;
            
            if (text == null)
            {
//--                trace("stateChange, onreadystatechange - Unable to Get Returned Data (" +aRequestId+ ")");
                aCallback(aRequestId, false, null);
                return;
            }

//--            trace("stateChange, onreadystatechange - Text Length: " + text.length+ " (" +aRequestId+ ")");
            aCallback(aRequestId, true, text);
            return;
        }

        var responseText = aHttpRequest.responseText;

//--        trace("stateChange, onreadystatechange - Unable to Get Url Status: " +aHttpRequest.status+ " (" +aRequestId+ ")");        
        aCallback(aRequestId, false, responseText);
    }
}


function sendGetRequest(aUrl, aRequestId, aHeaders, aCallback)
{
//--    trace("sendGetRequest, Url: " +aUrl);   

    if (aCallback == null)
    {
        error("sendGetRequest, No call back specified");
        return;
    }

    var httpRequest = null

    if (window.XMLHttpRequest)
    {
//--        trace("sendGetRequest - Window has XMLHttpRequest");
        // code for IE7+, Firefox, Chrome, Opera, Safari
        httpRequest = new XMLHttpRequest();
    }
    else
    {
//--        trace("sendGetRequest - Window does NOT have XMLHttpRequest");
        // code for IE6, IE5
        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
    }

    httpRequest.onreadystatechange = mkStateChangeFn(httpRequest, aRequestId, aCallback);

    var queryPos   = aUrl.indexOf("?");
    var concatChar = queryPos == -1 ? "?" : ":";

    var dateNow = new Date();
    var timeNow = dateNow.getTime();

    aUrl += concatChar+ "time="  +timeNow;
    
    httpRequest.open("GET", aUrl, true);

    if (aHeaders != null)
    {
        for (var headerKey in aHeaders)
        {
            var headerData = aHeaders[headerKey];

//--            trace("sendGetRequest, Header: " +headerKey+ ", Value: " +headerData);

            httpRequest.setRequestHeader(headerKey, headerData);
        }
    }    

    httpRequest.send();
}

function sendPostRequest(aUrl, aRequestId, aHeaders, aData, aCallback)
{
//--    trace("sendPostRequest, Url: " +aUrl);   
 
    if (aCallback == null)
    {
        error("sendPostRequest, No call back specified");
        return;
    }

    var httpRequest = null

    if (window.XMLHttpRequest)
    {
        //trace("sendPostRequest - Window has XMLHttpRequest");
        // code for IE7+, Firefox, Chrome, Opera, Safari
        httpRequest = new XMLHttpRequest();
    }
    else
    {
        //trace("sendPostRequest - Window does NOT have XMLHttpRequest");
        // code for IE6, IE5
        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
    }

    httpRequest.onreadystatechange = mkStateChangeFn(httpRequest, aRequestId, aCallback);

    var queryPos  = aUrl.indexOf("?");
    var concatChar = queryPos == -1 ? "?" : ":";

    var dateNow = new Date();
    var timeNow = dateNow.getTime();

    //aUrl += concatChar+ "time="  +timeNow;    

    httpRequest.open("POST", aUrl, true);

    if (aHeaders != null)
    {
        for (var headerKey in aHeaders)
        {
            var headerData = aHeaders[headerKey];

//--            trace("sendPostRequest, Header: " +headerKey+ ", Value: " +headerData);

            httpRequest.setRequestHeader(headerKey, headerData);

        }
    }
    
//--    trace("sendPostRequest, Data: " +aData);

    httpRequest.send(aData);
}

/*
Return the field object from the array that has the matching settingitem value
Returns null if there is no matching field in the array
*/
function getFieldUsingSettingItem(aResponseData, aSettingItemValue) {
    //trace(">> getFieldUsingSettingItem : aSettingItemValue: " + aSettingItemValue);
    var matchingField = null,
        currentField,
        currentSettingItemValue,
        fieldIndex,
        fieldArray = aResponseData.fields;

    if ((fieldArray === null) || (fieldArray === undefined)) {
//--        trace("Could not find field array");
    } else {
        for (fieldIndex = 0; fieldIndex < fieldArray.length; fieldIndex = fieldIndex + 1) {
            currentField = fieldArray[fieldIndex];

            if ((currentField !== null) && (currentField !== undefined)) {
                currentSettingItemValue = currentField.fieldid;

                if ((currentSettingItemValue !== null) && (currentSettingItemValue !== undefined)) {
                    if (currentSettingItemValue === aSettingItemValue) {
//--                        trace("Got matching field: " + JSON.stringify(currentField));
                        matchingField = currentField;
                        break;
                    }
                }
            }
        }
    }

//--    trace("<< getFieldUsingSettingItem");

    return matchingField;
}
    
// Utility function to search for a field and get the fieldValue as a percentage
// Returns null if any of the following conditions are true:
    // - the field or value could not be found
    // - there is no maximum value (minimum values are assumed to be 0)
function getPercentValueFromResponseData(aResponseData, aSettingItemValue) {
//--    trace(">> getValueFromResponseData");

    var matchingField,
        matchingMaximumNumber = NaN,
        matchingValueNumber = NaN,
        percentValue = null;

    matchingField = getFieldUsingSettingItem(aResponseData, aSettingItemValue);

    if ((matchingField !== null) && (matchingField !== undefined)) {
        matchingValueNumber = Number(matchingField.fieldvalue);
        matchingMaximumNumber = Number(matchingField.fieldmaxvalue);
    }
    
    if ((isNaN(matchingMaximumNumber)) || (isNaN(matchingValueNumber))) {
        trace("Can't get percentage value because there's no value, or no max value");
        percentValue = null;
    } else {
        percentValue = (matchingValueNumber / matchingMaximumNumber) * 100;
        percentValue = Math.ceil(percentValue);
        if (percentValue > 100) {
            percentValue = 100;
        }
    }
    
    return percentValue;
}

// Utility function to search for a field and get the fieldValue
// Returns null if the field or value could not be found, otherwise it returns the value of the field
function getValueFromResponseData(aResponseData, aSettingItemValue) {
//--    trace(">> getValueFromResponseData");

    var matchingField,
        matchingValue = null;

    matchingField = getFieldUsingSettingItem(aResponseData, aSettingItemValue);

    if ((matchingField !== null) && (matchingField !== undefined)) {
        matchingValue = matchingField.fieldvalue;

        if (matchingValue === undefined) {
            matchingValue = null;
        }
    } else {
//--        trace("No matching field");
    }

//--    trace("<< getValueFromResponseData");

    return matchingValue;
}

    function getUpdateFieldValue(aResponse, aNodeId, aFieldId)
    {
        if (aResponse == null || aResponse.changedNodes == null)
        {
            trace("getUpdateFieldValue, Response does not contain list of nodes");
            return null;
        }

        var nodeList = aResponse.changedNodes;
        var node     = null;

        for (var nodeIndex in nodeList)
        {
            var thisNode = nodeList[nodeIndex];

            var thisNodeId = thisNode.nodeid;

            if (thisNodeId == aNodeId)
            {
                node = thisNode;
                break;
            }
        }

        if (node == null)
        {
            trace("getUpdateFieldValue, Response does not contain node: " +aNodeId);
            return null;
        }

        var fieldList= node.fields;

        if (fieldList == null)
        {
            trace("getUpdateFieldValue, Node does not contain list of fields, Node: " +aNodeId);
            return null;            
        }

        var field = null;

        for (var fieldIndex in fieldList)
        {
            var thisField = fieldList[fieldIndex];

            if (thisField.fieldid == aFieldId)
            {
                field = thisField
                break;
            }
        }

        if (field == null)
        {
            trace("getUpdateFieldValue, Node: " +aNodeId+ " does not contain field: " +aField);
            return null;
        }        

        var fieldValue = field.value;

        trace("getUpdateFieldValue, Node: " +aNodeId+ ", Field: " +aFieldId+ ", Value: " +fieldValue);

        return fieldValue;
    }

//===================================================================================================
// IMEI Utilities
//===================================================================================================
//
function makeDeviceImei(aNetworkImei)
{
    if (aNetworkImei == null || aNetworkImei.length < 14)
    {
        trace("makeDeviceImei, Invalid IMEI: " +aNetworkImei);
        return null;
    }

    var baseImei = aNetworkImei.substr(0, 14);

    //trace("makeDeviceImei, Base IMEI: " +baseImei);

    var deviceImei = null;

    try
    {
        var total = 0;

        for (var index = 0; index < 14; index++)
        {
            var thisChar = baseImei.charAt(index);

            var thisDigit = parseInt(thisChar, 10);

            if (isNaN(thisDigit))
            {
                trace("makeDeviceImei, Invalid IMEI: " +aNetworkImei);
                return null;
            }

            // Double the digits at odd indicies (e.g, 1, 3, 5, ...)
            //
            if ((index%2) == 1)
            {
                //trace("makeDeviceImei, Doubling: " +thisDigit+ " (Index: " +index+ ")");
                thisDigit *= 2; 
            }

            // If number is greater or eqaul to 10 we add its digits separately (eg for 18 we add 1 + 8 to the total)
            // The number will be at most 18 so we can take a short cut and just subtract 10 and add 1
            //
            if (thisDigit >= 10)
            {
                thisDigit -= 10;
                thisDigit += 1;
            }

            total += thisDigit;
        }

        var remainder = total % 10;

        var checkSum = (10 - remainder) % 10;

        //trace("makeDeviceImei, Total: " +total+ ", Remainder: " +remainder+ ", CheckSum: " +checkSum);

        deviceImei = baseImei + "" + checkSum;

        //trace("makeDeviceImei, Network: " +aNetworkImei+ ", Base: " +baseImei+ ", Device: " +deviceImei);
    }
    catch (err)
    {
        error("makeDeviceImei, Exception: " +err);
        deviceImei = null;
    }

    return deviceImei;
}

//===================================================================================================
// Date Utilities
//===================================================================================================
//
// Telstra dates might be missing a colon in the timezone designator part
// I.e. they are in the form 2016-05-26T08:50:00+1000 but should be 2016-05-26T08:50:00+10:00
//
function parseDate(aDate)
{
    //trace("parseDate, Date: " +aDate);

    var timeDesignator = aDate.indexOf("T");

    if (timeDesignator == -1)
    {
        var date = Date.parse(aDate);
        //trace("parseDate, No Time Part: " +date);
        return date;
    }        

    var timeZoneDesignator = aDate.indexOf("+", timeDesignator);

    if (timeZoneDesignator == -1)
    {
        timeZoneDesignator = aDate.indexOf("-", timeDesignator);       
    }

    if (timeZoneDesignator == -1)
    {
        var date = Date.parse(aDate);
        //trace("parseDate, No TZD: " +date);
        return date;
    }

    var colonPos = timeZoneDesignator + 3;

    var colon = aDate.substr(colonPos, 1);

    if (colon != ":")
    {
        var part1 = aDate.substr(0, colonPos);
        var part2 = aDate.substr(colonPos);

        //trace("parseDate, Part1: " +part1+ ", Part2: " +part2);

        aDate = part1+ ":" +part2;            
    }

    var date = Date.parse(aDate);

    //trace("parseDate, Time2: " +date);

    return date;
}

// Parse European / Australian date in format dd/mm/yy

function parseDDMMYYDate(aDateStr)
{
    if (aDateStr == null)
    {
        error("parseDDMMYYDate, Null Date Passed");
        return null;
    }

    var fields = aDateStr.split('/');

    if (fields.length != 3)
    {
        error("parseDDMMYYDate, Bad Date: " +aDateStr)
        return null;
    }

    var day   = fields[0] - 0;
    var month = fields[1] - 1; //Javascript months are 0-11
    var year  = fields[2] - 0;

    if (year < 100)
    {
        year += 2000;
    }

    var date = new Date(year, month, day);

    return date;
}


function makeShortDateString(aDate)
{
    var day   = aDate.getDate()     - 0;
    var month = aDate.getMonth()    - 0;
    var year  = aDate.getFullYear() - 0;

    month = month + 1;
    year  = year % 100;

    var dateStr = day+ "/" +month+ "/" +year;

    return dateStr;
}    

function makeDateString(aDate)
{
    var day   = aDate.getDate()     - 0;
    var month = aDate.getMonth()    - 0;
    var year  = aDate.getFullYear() - 0;

    month = month + 1;

    var dateStr = day+ "/" +month+ "/" +year;

    return dateStr;
}       

function makeStandardDateString(aDate)
{
    var day   = aDate.getDate()     - 0;
    var month = aDate.getMonth()    - 0;
    var year  = aDate.getFullYear() - 0;

    month = month + 1;

    if (day < 10)
    {
        day = "0" +day;
    }

    if (month < 10)
    {
        month = "0" +month;
    }

    var dateStr = year+ "-" +month+ "-" +day;

    return dateStr;
}   

function makeMMDDYYYYDateString(aDate)
{
    var day   = aDate.getDate()     - 0;
    var month = aDate.getMonth()    - 0;
    var year  = aDate.getFullYear() - 0;

    month = month + 1;

    if (day < 10)
    {
        day = "0" +day;
    }

    if (month < 10)
    {
        month = "0" +month;
    }

    var dateStr = month+ "/" +day+ "/" +year;

    return dateStr;
}  


function makeDayDateString(aDate)
{
    var dayOfWeek  = aDate.getDay();
    var dayOfMonth = aDate.getDate();    

    var month = aDate.getMonth();
    var year  = aDate.getFullYear();

    var dateStr = DAY_NAMES[dayOfWeek]+ ", " +dayOfMonth+ " " +MONTH_NAMES[month];

    var dateNow = new Date();
    var yearNow = dateNow.getFullYear();

    if (year != yearNow)
    {
        dateStr += " " +year;
    }

    return dateStr;
} 

//===================================================================================================
// Value Store Utilities
//===================================================================================================
//

function getStoreValue(aStore, aKey)
{
    if (aStore == null || aKey == null)
    {
        error("getStoreValue, No Store and/or Key supplied: " +aStore+ "/" +aKey);
        return null;
    }

    var bridge    = getDeviceBridge();
    var storeData = bridge.getValue(aStore);

    var value = null;

    if (storeData != null && storeData != "")
    {
        try
        {
            store = JSON.parse(storeData)

            value = store[aKey];
        }   
        catch (err)
        {
            error("getStoreValue, Could not decode store: " +err);
            value = null;
        }         
    }

    trace("getStoreValue, Key: " +aKey+ ", Value: " +value);   

    return value;  
}

function setStoreValue(aStore, aKey, aValue)
{
    trace("setStoreValue, Store: " +aStore+ ", Key: " +aKey+ ", Value: " +aValue);   

    if (aStore == null || aKey == null)
    {
        error("setStoreValue, No Store and/or Key supplied: " +aStore+ "/" +aKey);
        return false;
    }

    var bridge    = getDeviceBridge();
    var storeData = bridge.getValue(aStore);

    var store = {};

    if (storeData != null && storeData != "")
    {
        try
        {
            store = JSON.parse(storeData)
        }   
        catch (err)
        {
            error("setStoreValue, Could not decode store: " +err);
            store = {};
        }         
    }

    store[aKey] = aValue;

    var newStoreData = JSON.stringify(store);

    bridge.setValue(aStore, newStoreData);  

    return true;
}

function clearStoreValues(aStore)
{
    trace("clearStoreValues, Store: " +aStore);   

    setStoreContents(aStore, null);
}

function getStoreContents(aStore)
{
    trace("getStoreContents, Store: " +aStore);   

    var bridge    = getDeviceBridge();
    var storeData = bridge.getValue(aStore);

    if (storeData == null || storeData == "")
    {
        return {};
    }

    var contents = {};

    try
    {
        contents = JSON.parse(storeData)
    }   
    catch (err)
    {
        error("getStoreContents, Could not decode store: " +err);
        contents = {};
    } 

    return contents;
}

function setStoreContents(aStore, aContents)
{
    trace("setStoreContents, Store: " +aStore);   

    var storeData = "";

    if (aContents != null)
    {
        storeData = JSON.stringify(aContents);
    }

    var bridge = getDeviceBridge();

    bridge.setValue(aStore, storeData);  
}


//===================================================================================================
// Exports
//===================================================================================================
//

var exports = {};

exports.name = "mmrsutils";

exports.doTrace = function(aTag, aText) { doTrace(aTag, aText); }
exports.doWarn  = function(aTag, aText) { doWarn (aTag, aText); }
exports.doError = function(aTag, aText) { doError(aTag, aText); }

exports.isIPhoneRun   = function() { return isIPhoneRun(); }
exports.isAndroidRun  = function() { return isAndroidRun(); }
exports.isNativeRun   = function() { return isNativeRun(); }

exports.getDeviceBridge = function() { return getDeviceBridge(); }

exports.copyData   = function(aObject) { return copyData(aObject); }

exports.contains   = function(aLine, aWord) { return contains(aLine, aWord); }

exports.setElement = function(aItemId, aContents) { setElement(aItemId, aContents); }

exports.getAngularScope = function(aAngularArea) { return getAngularScope(aAngularArea); }

exports.getFieldValue  = function() { return getFieldValue.apply(null, arguments); }

exports.getOptions = function() { return getOptions(); }
exports.getUrlPath = function() { return getUrlPath(); }

exports.getRequestUrl = function(aHostUrl, aUrl) { return getRequestUrl(aHostUrl, aUrl); }

exports.getElementPosition  = function(aElementId) { return getElementPosition(aElementId); }

exports.getScreenDimensions = function() { return getScreenDimensions(); }

exports.sendGetRequest  = function(aUrl, aRequestId, aHeaders, aCallback)        { return sendGetRequest (aUrl, aRequestId, aHeaders, aCallback); }
exports.sendPostRequest = function(aUrl, aRequestId, aHeaders, aData, aCallback) { return sendPostRequest(aUrl, aRequestId, aHeaders, aData, aCallback); }

exports.getValueFromResponseData = function(aResponseData, aSettingItemValue) {return getValueFromResponseData(aResponseData, aSettingItemValue); }

exports.getUpdateFieldValue = function(aResponse, aNodeId, aFieldId) { return getUpdateFieldValue(aResponse, aNodeId, aFieldId); }

exports.getPercentValueFromResponseData = function(aResponseData, aSettingItemValue) {return getPercentValueFromResponseData(aResponseData, aSettingItemValue); }

exports.getFieldUsingSettingItem = function(aResponseData, aSettingItemValue) {return getFieldUsingSettingItem(aResponseData, aSettingItemValue); }

exports.makeDeviceImei = function(aNetworkImei) { return makeDeviceImei(aNetworkImei); }

exports.getStoreValue    = function(aStore, aKey)         { return getStoreValue(aStore, aKey);         }
exports.setStoreValue    = function(aStore, aKey, aValue) { return setStoreValue(aStore, aKey, aValue); }
exports.clearStoreValues = function(aStore)               { return clearStoreValues(aStore);            }
exports.getStoreContents = function(aStore)               { return getStoreContents(aStore);            }
exports.setStoreContents = function(aStore, aContents)    { return setStoreContents(aStore, aContents); }

exports.parseDate = function(aDateStr) { return parseDate(aDateStr); }

exports.parseDDMMYYDate = function(aDateStr) { return parseDDMMYYDate(aDateStr); }

exports.makeShortDateString    = function(aDate) { return makeShortDateString(aDate); }
exports.makeDateString         = function(aDate) { return makeDateString(aDate); }
exports.makeStandardDateString = function(aDate) { return makeStandardDateString(aDate); }
exports.makeDayDateString      = function(aDate) { return makeDayDateString(aDate); }
exports.makeMMDDYYYYDateString = function(aDate) { return makeMMDDYYYYDateString(aDate); }


return exports;

}());
