var JSONCOMPARE = (function(){

function trace(aText) { MMRSUTILS.doTrace(exports.name, aText); }
function warn(aText)  { MMRSUTILS.doWarn (exports.name, aText); }
function error(aText) { MMRSUTILS.doError(exports.name, aText); }

//===================================================================================================
// Object Utilities
//===================================================================================================
//
function compare(aObject1, aObject2, aIgnore)
{
    var object1 = null;
    var object2 = null;
    
    try
    {
        object1 = JSON.parse(aObject1);
        object2 = JSON.parse(aObject2);
    }
    catch(err)
    {
        error("compare, Exception: " +err);
    }

    if (object1 == null || aObject2 == null)
    {
        return null;
    }

    var items1 = {};
    var items2 = {};

    expandObject(object1, "", items1);
    expandObject(object2, "", items2);

    console.log(items1);

    var diffs = compareObjects(items1, items2);

    console.log(diffs);

    return diffs;
}

function expandObject(aObject, aPrefix, aItems)
{
    var keys = Object.keys(aObject);

    keys.sort();

    for (var index in keys)
    {
        var thisKey   = keys[index];
        var thisValue = aObject[thisKey];

        var thisPath  = aPrefix + thisKey;

        //trace("expandObject, Path: " +thisPath);

        if (thisValue == null)
        {
            aItems[thisPath] = thisValue;
            continue;
        }

        if (Array.isArray(thisValue))
        {
            var subPrefix = thisPath + ".";

            expandArray(thisValue, subPrefix, aItems);
            continue;
        }

        if (typeof thisValue === 'object')
        {
            var subPrefix = thisPath + ".";

            expandObject(thisValue, subPrefix, aItems);
            continue;
        }

        aItems[thisPath] = thisValue;
    }
}

function expandArray(aArray, aPrefix, aItems)
{    
    for (var index in aArray)
    {        
        var thisValue = aArray[index];

        var thisPath  = aPrefix + index;

        if (thisValue == null)
        {
            aItems[thisPath] = thisValue;
            continue;
        }

        if (Array.isArray(thisValue))
        {
            var subPrefix = thisPath + ".";

            expandArray(thisValue, subPrefix, aItems);
            continue;
        }

        if (typeof thisValue === 'object')
        {
            var subPrefix = thisPath + ".";

            expandObject(thisValue, subPrefix, aItems);
            continue;
        }  

        aItems[thisPath] = thisValue;
    }
}

function compareObjects(aObject1, aObject2)
{
    var diffs = [];

    for (var key in aObject1)
    {
        var value1 = aObject1[key];
        var value2 = aObject2[key];

        //trace("compareObjects, Key: " +key+ ", Left: " +value1+ ", Right: " +value2);

        if (value2 == null)
        {
            var thisDiff = {key: key, left: value1, right: ""};
            diffs.push(thisDiff);
            continue;
        }

        if (value1 != value2)
        {
            var thisDiff = {key: key, left: value1, right: value2};
            diffs.push(thisDiff);
            continue;
        }
    }

    for (var key in aObject2)
    {
        var value1 = aObject1[key];
        var value2 = aObject2[key];

        if (value1 == null)
        {
            var thisDiff = {key: key, left: "", right: value2};
            diffs.push(thisDiff);
            continue;
        }
    }

    return diffs;
}


//===================================================================================================
// Exports
//===================================================================================================
//

var exports = {};

exports.name = "jsoncompare";


exports.compare   = function(aObject1, aObject2, aIgnore) { return compare(aObject1, aObject2, aIgnore); }

return exports;

}());
