var CMDPLAYER = (function(){

var DEVICE_URL = "http://localhost:31313/";

var PAGE_RETRY_ATTEMPTS      = 5;
var PAGE_RETRY_ATTEMPTS_LONG = 10;

var PAGE_RETRY_TIME     = 2000;  // Milli-Seconds   //  Wait 2 seconds before re-checking page-id 

var COMMAND_PAUSE_TIME    = 3000;  // Milli-Seconds   // How long to wait after successfully sending a command
var COMMAND_RETRY_TIME    = 2000;  // Milli-Seconds   // How long to wait before re-trying the same comment.
var COMMAND_RETRY_DEFAULT = 1;     // How many times to retry a command by default


var BRIDGE = MMRSUTILS.getDeviceBridge();

function trace(aText) { MMRSUTILS.doTrace(exports.name, aText); }
function warn(aText)  { MMRSUTILS.doWarn (exports.name, aText); }
function error(aText) { MMRSUTILS.doError(exports.name, aText); }


    function playCommands(aCommands, aIndex, aCallback)
    {
        trace("playCommands, Command Index: " +aIndex);        

        if (aIndex >= aCommands.length)
        {
            trace("playCommands, All commmands sent - fetching outcome");        

            fetchValue("rr-submit-request", function(aValue)
            {
                aCallback(true, aValue);
            });
            return;
        }

        var command = aCommands[aIndex];

        sendCommandToPage(command, function(aSuccess, aResponse)
        {
            if (!aSuccess)
            {
                trace("playCommands, Command Failed. Index: " +aIndex);
                aCallback(false);
                return;
            }

            trace("playCommands, Command Succeeded. Index: " +aIndex);

            aIndex++;

            var pauseTime = command.pause == null ? COMMAND_PAUSE_TIME : command.pause*1000;

            setTimeout(playCommands, pauseTime, aCommands, aIndex, aCallback);
        }); 
    }

    function fetchValue(aValueKey, aCallback)
    {
        var valueUrl = DEVICE_URL + "value/" +aValueKey;

        MMRSUTILS.sendGetRequest(valueUrl, "rqst-value", null, function(aRequestId, aSuccess, aResponse)
        {
            //trace("fetchValue, Rqst: " +aRequestId+ ", Success: " +aSuccess+ ", Response: " +aResponse);

            aCallback(aResponse);
        });
    }


    function sendCommandToPage(aCommand, aCallback)
    {
        trace("sendCommandToPage");

        if (aCommand.pageId == null)
        {
            error("sendCommandToPage, Command does not have a pageId");
            return;
        }

        if (aCommand.element == null)
        {
            error("sendCommandToPage, Command does not have an element");
            return;
        }        


        waitForPage(aCommand, function(aSuccess, aPageInfo)
        {
            trace("sendCommandToPage, On Page: " +aSuccess);

            if (!aSuccess)
            {
                error("sendCommandToPage, Not on desired page");
                aCallback(false, null);
                return;
            }

            var retries = aCommand.retries == null ? COMMAND_RETRY_DEFAULT : aCommand.retries;

            sendCommand(aCommand, retries, aCallback);
        });
    }

    function sendCommand(aCommand, aRetries, aCallback)
    {
        trace("sendCommand, Command Tries: " +aRetries);

        var commandStr = JSON.stringify(aCommand);

        MMRSUTILS.sendPostRequest(DEVICE_URL, "cmd-rqst", null, commandStr, 
        function(aRequestId, aSuccess, aResponse)
        {
            if (aSuccess)
            {
                aCallback(true, aResponse);
                return;
            }

            if (aRetries <= 0)
            {                
                warn("sendCommand, Command Failed: " +aResponse);
                aCallback(false, aResponse);
                return;
            }

            var retries = aRetries - 1;

            setTimeout(sendCommand, COMMAND_RETRY_TIME, aCommand, retries, aCallback);
        });
    }

    function onPostResponse(aRequestId, aSuccess, aResponse)
    {
        trace("onPostResponse, ID: " +aRequestId+ ", Success: " +aSuccess+ ", Response: " +aResponse);
    } 

    function waitForPage(aCommand, aCallback, aCount)
    {
        trace(">> waitForPage")

        if (aCount == null)
        {
            aCount = 0;
        }

        var wantedPageId = aCommand.pageId;

        var maxRetries = aCommand.retries == null ? PAGE_RETRY_ATTEMPTS : aCommand.retries;

        MMRSUTILS.sendGetRequest(DEVICE_URL, "page-rqst", null,
            function(aRequestId, aSuccess, aResponse)
            {
                trace("waitForPage, Success: " +aSuccess);

                if (!aSuccess)
                {
                    aCallback(false, null);
                    return;
                }

                trace("waitForPage, Response: " +aResponse);

                try
                {
                    var pageInfo = JSON.parse(aResponse);

                    var currentPageId = pageInfo.pageId;

                    trace("waitForPage, Wanted: " +wantedPageId+ ", Current: " +currentPageId);                    

                    var pageIdExpr = new RegExp("^" +wantedPageId+ "$", "i");
                    
                    if (currentPageId.match(pageIdExpr))
                    {
                        aCallback(true, pageInfo);
                        return;
                    }

                    aCount++;

                    if (aCount >= maxRetries)
                    {
                        error("waitForPage, Not on desired page - Wanted: " +wantedPageId+ ", Current: " +currentPageId);
                        aCallback(false, null);
                        return;
                    }

                    setTimeout(waitForPage, PAGE_RETRY_TIME, aCommand, aCallback, aCount);
                }
                catch (err)
                {
                    error("waitForPage, Exception: " +err);
                    aCallback(false, null);                
                }

                trace("<< waitForPage")
            });
    }




//===================================================================================================
// Exports
//===================================================================================================
//

var exports = {};

exports.name = "cmdplayer";

exports.PAGE_RETRY_ATTEMPTS_LONG = PAGE_RETRY_ATTEMPTS_LONG;

exports.playCommands = function(aCommands, aIndex, aCallback) { return playCommands(aCommands, aIndex, aCallback); }


return exports;

}());
