var MMRSAUTOMATOR = (function() {
	"use strict"

function trace(aText) { MMRSUTILS.doTrace(exports.name, aText); }
function warn(aText)  { MMRSUTILS.doWarn (exports.name, aText); }
function error(aText) { MMRSUTILS.doError(exports.name, aText); }

var PAGE_RETRY_ATTEMPTS_LONG = CMDPLAYER.PAGE_RETRY_ATTEMPTS_LONG;

window.onerror = function(aMessage) { error(aMessage); }

var mTheApp = angular.module('aApp', ['ngSanitize']);

mTheApp.controller('aController', function($scope, $timeout)
{
    var ICON_ACTIVE = "images/test-active.gif";
    var ICON_PASSED = "images/test-passed.png";
    var ICON_FAILED = "images/test-failed.png";

    $scope.menuItems =
    [
        {
            icon:    "",
            title:   "Outside Warranty",
            comment: "Fault, No Damage, Warranty Expired",
            flow:    CMDFLOWS.FAULT_NO_DAMAGE_NO_WARRANTY
        },    
        {
            icon:    "",
            title:   "Forward Ship",
            comment: "Fault, No Damage, Warranty, In Stock",
            flow:    CMDFLOWS.FAULT_NO_DAMAGE_WARRANTY_STOCK
        },  
        {
            icon1:   "",
            title:   "Warranty Replacement",
            comment: "Fault, No Damage, Warranty, Out Of Stock",
            flow:    CMDFLOWS.FAULT_NO_DAMAGE_WARRANTY_NO_STOCK
        },  
        {
            icon1:   "",
            title:   "Warranty Void Replacement",
            comment: "Fault, Damage, Warranty",
            flow:    CMDFLOWS.FAULT_DAMAGE_WARRANTY
        } 
    ];

    $scope.onMenuItemClick = function(aItem)
    {
        trace("onMenuItemClick, Item: " +aItem.title);

        var flow = aItem.flow;

        var commands = flow.commands;

        $scope.$applyAsync(function()
        {
            aItem.icon = ICON_ACTIVE;
        });

        fetchExpectedOutput(aItem, function(aExpectedOutput)
        {
            if (aExpectedOutput == null)
            {
                warn("onMenuItemClick, No expected output for option: " +aItem.title);
            }       

            //checkOutput(aItem, true, aExpectedOutput, aExpectedOutput);
            //return;


            CMDPLAYER.playCommands(commands, 0, function(aSuccess, aResponse)
            {
                trace("onMenuItemClick, Commands Sent. Success: " +aSuccess);                    
                trace("onMenuItemClick, Commands Sent. Output:  " +aResponse);  

                $scope.$applyAsync(function()
                {
                    aItem.icon = aSuccess ? ICON_PASSED : ICON_FAILED;
                });                

                if (aSuccess && aExpectedOutput != null)
                {
                    checkOutput(aItem, aSuccess, aExpectedOutput, aResponse);
                }
            });

        });
    }

    function fetchExpectedOutput(aItem, aCallback)
    {
        if (aItem.flow == null || aItem.flow.expectedOutput == null)
        {
            error("fetchExpectedResults, No expected results file specified in flow");
            aCallback(null);
            return;
        }

        var expectResultsUrl = aItem.flow.expectedOutput;                

        MMRSUTILS.sendGetRequest(expectResultsUrl, "rqst-expected", null, function(aRequestId, aSuccess, aResponse)
        {
            trace("fetchExpectedResults, Rqst: " +aRequestId+ ", Success: " +aSuccess+ ", Response: " +aResponse);

            aCallback(aResponse);
        });
    }


    function checkOutput(aItem, aSuccess, aExpectedOutput, aReceivedOutput)
    {
        var diffs = JSONCOMPARE.compare(aExpectedOutput, aReceivedOutput, {});

        //var diffs = [ {key: "Key", left: "Expected", right: "Actual"} ];

        $scope.$applyAsync(function()
        {
            aItem.diffs = diffs;
            return;            
        }); 

    }

    function getTimeNow()
    {
        var dateNow = new Date();
        var timeNow = dateNow.getTime();

        return timeNow;
    }

    ///////////////////////////////////////////////////////////////////////////////////////

 	$scope.doLoad = function()
    {
	    //trace(">> doLoad");

	    //trace("<< doLoad");
	}

    $scope.doLoad();



});

function onLoad()
{
}

var exports = {};

exports.name = "automator";

exports.onLoad = function(aOptions) { onLoad(aOptions); }

return exports;

}());
